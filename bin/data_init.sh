#!/bin/bash
set -e

SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

PROJECTDIR=${SCRIPTDIR}/..

# Date
DATE="`date '+%Y%m%d'`"

DEV=false
DOCKER_DATABASE=prodige_database
DOCKER_LDAP=jpr_prodige_cas_ldap
DOCKER_ADMINCARTO=ppr_prodige_admincarto_web

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                  : Help\n"
    printf "\t$SCRIPTNAME -p <pr_prodige folder> | --prodige-folder    : The folder where the pr_prodige project is launched\n"
    printf "\t$SCRIPTNAME --dev                                        : Use --dev for run script make_init_and_rights.sh\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################

function check_prodige_up {
  if [ "" != "`cd ${PR_PRODIGE_FOLDER}; docker-compose ps --services --filter "status=running" 2>/dev/null`" ]
  then
    logOkStep "Prodige is up"
  else
    logErrorStep "You must run prodige before run this script"
    exit 1
  fi
}

function rsync_data_init {
  rsync -rl ${PROJECTDIR}/data/ ${PR_PRODIGE_FOLDER}/data/
  rsync -rl ${PROJECTDIR}/var/ ${PR_PRODIGE_FOLDER}/var/
}

function import_postgres {
  chmod o+r ${PR_PRODIGE_FOLDER}/var/database/restore/*
  logOkStep "Restart database before"
  docker restart ${DOCKER_DATABASE}
  sleep 3
  logOkStep "Catalogue"
  docker exec -t -u postgres ${DOCKER_DATABASE} dropdb --if-exists catalogue
  docker exec -t -u postgres ${DOCKER_DATABASE} psql -q --echo-errors -f /backup/PGSQL_catalogue.sql
  logOkStep "Prodige"
  docker exec -t -u postgres ${DOCKER_DATABASE} dropdb --if-exists prodige
  docker exec -t -u postgres ${DOCKER_DATABASE} psql -q --echo-errors -f /backup/PGSQL_prodige.sql
}

function import_ldap {
  docker stop ${DOCKER_LDAP}
  docker rm ${DOCKER_LDAP}
  #rm -rf ${PR_PRODIGE_FOLDER}/data/openldap/config/*
  rm -rf ${PR_PRODIGE_FOLDER}/var/openldap/database/*
  cat <<EOF > ${PR_PRODIGE_FOLDER}/import_ldap.yml
version: '3.5'
services:
  ${DOCKER_LDAP}:
    command: [ ]
    entrypoint: [ "sleep", "30000" ]
EOF
  cd ${PR_PRODIGE_FOLDER}
  docker-compose -f docker-compose.yml -f import_ldap.yml up -d jpr_prodige_cas_ldap
  #docker exec -t ${DOCKER_LDAP} slapadd -F /etc/ldap/slapd.d -b cn=config -l '/restore/LDAP_cn=config.ldif'
  docker exec -t ${DOCKER_LDAP} slapadd -F /etc/ldap/slapd.d -b dc=alkante,dc=domprodige -l /restore/LDAP_dc\=alkante\,dc\=domprodige.ldif
  docker exec -t ${DOCKER_LDAP} chown -R openldap:openldap /var/lib/ldap
  docker exec -t ${DOCKER_LDAP} chown -R openldap:openldap /etc/ldap
#   cat <<EOF > ${PR_PRODIGE_FOLDER}/data/openldap/config/docker-openldap-was-started-with-tls
# export PREVIOUS_LDAP_TLS_CA_CRT_PATH=/container/run/service/slapd/assets/certs/ca.crt
# export PREVIOUS_LDAP_TLS_CRT_PATH=/container/run/service/slapd/assets/certs/ldap.crt
# export PREVIOUS_LDAP_TLS_KEY_PATH=/container/run/service/slapd/assets/certs/ldap.key
# export PREVIOUS_LDAP_TLS_DH_PARAM_PATH=/container/run/service/slapd/assets/certs/dhparam.pem
# EOF
  docker-compose up -d ${DOCKER_LDAP}
  rm -f ${PR_PRODIGE_FOLDER}/import_ldap.yml
}

function set_rights {
  if ${DEV}
  then
    logOkStep "Run make_init_and_rights.sh with --dev"
    ${PR_PRODIGE_FOLDER}/bin/make_init_and_rights.sh --dev
  else
    logOkStep "Run make_init_and_rights.sh"
    ${PR_PRODIGE_FOLDER}/bin/make_init_and_rights.sh
  fi
}

function restart_prodige {
  cd ${PR_PRODIGE_FOLDER}
  docker-compose restart
  logOkStep "Waiting 10 seconds"
  sleep 10
}

function generate_context {
  docker exec -u www-data -w /var/www/html/site ${DOCKER_ADMINCARTO} php bin/console carmen:generateContext 1
}

########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h,p: -l help,prodige-folder:,dev -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        -p | --prodige-folder)
        PR_PRODIGE_FOLDER=$2
        shift 2
        ;;
        --dev)
        DEV=true
        shift
        ;;
        --)
        shift
        break
        ;;
    esac
done

if [[ $(id -u) -ne 0 ]] ; then
    logError "You must be root or sudo to execute $SCRIPTNAME"
    exit 1
fi

if [ -z ${PR_PRODIGE_FOLDER} ]
then
    logError "pr_prodige folder not defined"
    usage
    exit 1
fi

# Check if git-lfs is necessary
if [ "true" != "$(cat ${PROJECTDIR}/bin/.lfs-check)" ]; then
  if ! [ -x "$(command -v git-lfs)" ]; then
    logError "Git lfs not installed, go to https://git-lfs.com/"
    exit 1
  fi
  if [ "true" != "$(cat ${PROJECTDIR}/bin/.lfs-check)" ]; then
    logError "Git lfs not pull: git lfs pull"
    exit 1
  fi
fi

# Step

NB_STEP_AL=7

STEP_AL=1
MODE="CheckProdigeUp"
logOkStep "Check prodige is up"
check_prodige_up

STEP_AL=2
MODE="RsyncDataInit"
logOkStep "Rsync data init"
rsync_data_init

STEP_AL=3
MODE="ImportPostgres"
logOkStep "Import Postgres"
import_postgres

STEP_AL=4
MODE="ImportLdap"
logOkStep "Import LDAP"
import_ldap

STEP_AL=5
MODE="SetRights"
logOkStep "Set rights"
set_rights

STEP_AL=6
MODE="RestartProdige"
logOkStep "Restart Prodige"
restart_prodige

STEP_AL=7
MODE="GenerateContext"
logOkStep "Generate context"
generate_context

logOk "End of ${SCRIPTNAME}"
logWarning "If you are in a production environment you must relaunch the scripts with the correct options:"
echo "./bin/move_prodige_url_data.sh"
echo "./bin/move_prodige_url_db.sh"
# End
