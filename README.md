# Données d'initialisation
Ce projet permet d'initialiser une plateforme PRODIGE avec quelques données libres et cartes interactives pré-construitres.

Vous devez exécuter le script bin/data_init.sh avec en paramètre le dossier du pr_prodige. Exemple :
```bash
sudo ./bin/data_init.sh -p ~/pr_prodige
```

Après l'import, le mot de passe par défaut est modifié, le nouveau mot de passe est ```mQ&Tw9@[(f?Yn^Gs```

Pour des raisons de sécurité, il est indispensable de modifier ce mot de passe après connexion.
